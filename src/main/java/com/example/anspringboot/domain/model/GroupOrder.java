package com.example.anspringboot.domain.model;

import javax.validation.GroupSequence;

/**
 * GroupOrder
 */
@GroupSequence({ValidGroup1.class,ValidGroup2.class,ValidGroup3.class})
public interface GroupOrder {

}
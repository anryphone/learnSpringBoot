package com.example.anspringboot.domain.repository;

import java.util.List;

import com.example.anspringboot.domain.model.User;

import org.springframework.dao.DataAccessException;

/**
 * UserDao
 */
public interface UserDao {
    // ポイント
    // Userテーブルの件数を獲得
    public int count() throws DataAccessException;
    
    // 1件挿入
    public int insertOne(User userId) throws DataAccessException;

    // 1件取得
    public User selectOne(String userId) throws DataAccessException;

    // 全件取得
    public List<User> selectMany() throws DataAccessException;

    // １件更新
    public int updateOne(User user) throws DataAccessException;

    // １件削除
    public int deleteOne(String userId) throws DataAccessException;

    // SQL取得結果をサーバにCSVで保存する
    public void userCsvOut() throws DataAccessException; 
}
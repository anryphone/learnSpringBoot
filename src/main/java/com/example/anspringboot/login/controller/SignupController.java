package com.example.anspringboot.login.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import com.example.anspringboot.domain.model.GroupOrder;
import com.example.anspringboot.domain.model.SignupForm;
import com.example.anspringboot.domain.model.User;
import com.example.anspringboot.domain.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class SignupController{

    @Autowired
    private UserService userService;

    // ラジオボタン要変数
    private Map<String,String> radioMarriage;

    // ラジオボタン初期化
    private Map<String,String> initRadioMarriage(){
        final Map<String, String> radio = new LinkedHashMap<>();

        // 既婚、未婚を設定
        radio.put("既婚", "true");
        radio.put("未婚", "false");

        return radio;
    }

    // ユーザー登録画面のGET用
    @GetMapping("/signup")
    public String getSingUp(@ModelAttribute final SignupForm form, final Model model) {

        // ラジオボタン初期化呼出
        radioMarriage = initRadioMarriage();

        // ラジオボタン用mapをModelに登録
        model.addAttribute("radioMarriage", radioMarriage);

        // signup.htmlに画面遷移
        return "login/signup";
    }

    // ユーザー登録画面のPOST用
    @PostMapping("/signup")
    public String postSignUp(@ModelAttribute @Validated(GroupOrder.class) final SignupForm form,
            final BindingResult bindingResult, final Model model) {

        // 入力チェック
        if(bindingResult.hasErrors()){
            return getSingUp(form, model);
        }
        // formの中身をコンソールに出して確認します
        System.out.println(form);

        //insert用変数
        final User user = new User();
        user.setUserId(form.getUserId());// ユーザーID
        user.setPassword(form.getPassword());// パスワード
        user.setUserName(form.getUserName());// ユーザー名
        user.setBirthday(form.getBirthday());// 誕生日
        user.setAge(form.getAge());// 年齢
        user.setMarriage(form.isMarriage());// 結婚ステータス
        user.setRole("ROLE_GENERAL");// ロール（一般）

        // ユーザー登録処理
        final boolean result = userService.insert(user);

        // ユーザー登録結果の判定
        if (result == true) {
            System.out.println("insert成功");
        } else {
            System.out.println("insert失敗");
        }
        return "redirect:/login";
    }

    // /**
    //  * DataAccessException発生時の処理メソッド.
    //  */
    // @ExceptionHandler(DataAccessException.class)
    // public String dataAccessExceptionHandler(final DataAccessException e, final Model model) {

    //     // 例外クラスのメッセージをModelに登録
    //     model.addAttribute("error", "内部サーバーエラー（DB）：ExceptionHandler");

    //     // 例外クラスのメッセージをModelに登録
    //     model.addAttribute("message", "SignupControllerでDataAccessExceptionが発生しました");

    //     // HTTPのエラーコード（500）をModelに登録
    //     model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR.value());

    //     return "error";
    // }

    // /**
    //  * Exception発生時の処理メソッド.
    //  */
    // @ExceptionHandler(Exception.class)
    // public String exceptionHandler(final Exception e, final Model model) {

    //     // 例外クラスのメッセージをModelに登録
    //     model.addAttribute("error", "内部サーバーエラー：ExceptionHandler");

    //     // 例外クラスのメッセージをModelに登録
    //     model.addAttribute("message", "SignupControllerでExceptionが発生しました");

    //     // HTTPのエラーコード（500）をModelに登録
    //     model.addAttribute("status", HttpStatus.INTERNAL_SERVER_ERROR.value());

    //     return "error";
    // }
}
package com.example.anspringboot.login.aspect;

import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.aspectj.lang.ProceedingJoinPoint;

@Aspect
@Component
public class LogAspect{
    //コントローラーのログ出力
    @Around("execution(* *..*.*Controller.*(..))")
    public Object startLog(final ProceedingJoinPoint jp) throws Throwable {
        System.out.println("メソッド開始：" + jp.getSignature());

        try {
            // メソッド実行
            final Object result = jp.proceed();
            System.out.println("メソッド終了：" + jp.getSignature());
            return result;
        } catch (final Exception e) {
            System.out.println("メソッド異常終了：" + jp.getSignature());
            e.printStackTrace();
            throw e;
        }
    }

    // UserDaoクラスのログ出力
    @Around("execution(* *..*.*UserDao*.*(..))")
    public Object daoLog(final ProceedingJoinPoint jp) throws Throwable{
        System.out.println("メソッド開始："  +  jp.getSignature());
        try{
            final Object result=jp.proceed();
            System.out.println("メソッド終了："  +  jp.getSignature());
            return result;
        }catch(final Exception e){
            System.out.println("メソッド異常終了："  +  jp.getSignature());
            e.printStackTrace();
            throw e;
        }
    }
}